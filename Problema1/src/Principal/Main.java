package Principal;

import static Principal.Main.id1;
import static Principal.Main.id2;
import static java.lang.Thread.sleep;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

class hilo1 extends Thread {
    Salida salida;
    Entrada entrada;
    CentroAcopioFrame frame;
    
    public hilo1(Entrada entrada, Salida salida,  CentroAcopioFrame frame) {
       this.salida = salida;
       this.entrada = entrada;
       this.frame = frame;
    }
    
    @Override
    public void run (){
        while (true) {
            try {
                Random r = new Random();
                int tiempo = (r.nextInt(5 - 0)) * 1000;
                sleep(tiempo);
                Thread entregaCaja = new EntregaCaja(id1++, entrada, salida, frame);
                entregaCaja.start();

            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}



class hilo2 extends Thread {
    Salida salida;
    Entrada entrada;
    CentroAcopioFrame frame;
    
    public hilo2(Entrada entrada, Salida salida,  CentroAcopioFrame frame) {
       this.salida = salida;
       this.entrada = entrada;
       this.frame = frame;
    }
    
    @Override
    public void run (){
        while (true) {
            try {
                Random r2 = new Random();
                int tiempo2 = (r2.nextInt(8 - 0)) * 1000;
                sleep(tiempo2);
                Thread recogidaCaja = new RecogidaCaja(id2++, entrada, salida, frame);
                recogidaCaja.start();
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

public class Main {

    static CentroAcopioFrame frame = new CentroAcopioFrame();
    static Integer casillas = 20;
    static Integer id1 = 1;
    static Integer id2 = 1;
    static Estante estante = new Estante(casillas, 0, 0);
    static Entrada entrada = new Entrada(estante, frame);
    static Salida salida = new Salida(estante, frame);
    
    public static void main(String[] args) {
        frame.show();
        new hilo1(entrada, salida, frame).start();
        new hilo2(entrada, salida, frame).start();
    }
}
