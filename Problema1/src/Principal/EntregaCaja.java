/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import java.net.URL;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author 
 */
public class EntregaCaja extends Thread {

    public Integer no_caja;
    public String estado;
    public Entrada entrada;
    public Salida salida;
    public CentroAcopioFrame frame;

    public EntregaCaja(Integer no_caja, Entrada entrada, Salida salida, CentroAcopioFrame frame) {
        this.no_caja = no_caja;
        this.estado = "Transitando";
        this.entrada = entrada;
        this.salida = salida;
        this.frame = frame;
    }

    @Override
    public void run() {
        try {
            this.entregar();
        } catch (InterruptedException e) {
            //System.out.println(e.getMessage());
        }
    }

    private void entregar() throws InterruptedException {
        this.entrada.meterCaja();
        this.frame.llenarEstante(no_caja.toString());
        this.estado = "En estante";
        System.out.println("La caja " + no_caja + " se coloco en el estante");
    }
    
}
