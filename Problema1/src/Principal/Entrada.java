/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

/**
 *
 * @author 
 */
public class Entrada {

    public Estante estante;
    public CentroAcopioFrame frame;
    
    public Entrada(Estante estante, CentroAcopioFrame frame) {
        this.estante = estante;
        this.frame = frame;
    }
    
    public synchronized void meterCaja() throws InterruptedException {
        this.estante.espera_entregar++;
        while (this.estante.casillas == 0) {
        }
        this.estante.espera_entregar--;
        this.estante.casillas--;
    }
    
}
