/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

/**
 *
 * @author
 */
public class Salida {
    public Estante estante;
    public CentroAcopioFrame frame;

    public Salida(Estante estante, CentroAcopioFrame frame) {
        this.estante = estante;
        this.frame = frame;
    } 
    
    public synchronized void sacarCaja(Integer no_caja) throws InterruptedException {
        this.estante.espera_rocoger++;
        while (!frame.isInside(no_caja.toString())) { 
            
        }
        this.estante.casillas++;
        this.estante.espera_rocoger--;
    }    
}
