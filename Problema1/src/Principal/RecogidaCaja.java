/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import java.net.URL;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author
 */
public class RecogidaCaja extends Thread {

    public Integer no_caja;
    public String estado;
    public Entrada entrada;
    public Salida salida;
    public CentroAcopioFrame frame;

    public RecogidaCaja(Integer no_caja, Entrada entrada, Salida salida, CentroAcopioFrame frame) {
        this.no_caja = no_caja;
        this.estado = "Transitando";
        this.entrada = entrada;
        this.salida = salida;
        this.frame = frame;
    }

    @Override
    public void run() {
        try {
            this.recoger();
        } catch (InterruptedException e) {
            //System.out.println(e.getMessage());
        }
    }

    private void recoger() throws InterruptedException {
        this.salida.sacarCaja(no_caja);
        this.frame.vaciarEstante(no_caja.toString());
        this.estado = "Retirada";
        System.out.println("La caja " + no_caja + " se retiro del estante");
    }
    
}
