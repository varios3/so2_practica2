# Centro de acopio
![Barberos Dormilones](https://www.extin-flam.com.mx/image/cache/catalog/extinflamproducts/SA018N08-228x228.jpg)


### Descripción de las Clases 📋


##### Estante
    Clase auxiliar del programa, que nos sirve para tener el control de 
    estantes libres y de personas en entrada como en salida, esta clase clase ademas es inicializada con el valor que se quiere iniciar el tamaño del estante.


##### EntregaCaja
    Clase en donde se encuentra el procedimiento de entrega, esta extiende de la clase thread ya que se crean multimples entregas de cajas a la hora de ejecutar el programa.

##### Entrada
    Esta clase es utilizada para crear una entrada, la cual tiene la acción de entregar la caja y cambiar los valores en el estante para llevar el control de cajas entregadas en el estante.

##### RecogidaCaja
    Clase en donde se encuentra el procedimiento de entrega de caja a personas necesiada, esta extiende la clase thread para la creación de multiples recogidas de cajas a la hora de la ejecución del programa.
    
##### Salida
    Clase para el manejo de la salida, esta clase contiene acciones para recoger una caja, y manejar los valores en el estante para el control de salida de cajas en el estante.
    
##### CentroAcopioFrame
    Clase que tiene toda la parte visual del programa, la cual dependen de todas las anteriores para su correcto funcionamiento y 
    visualización.
    
### Flujo General de la Aplicación
![Flujo](./Contenido/Flujo.png)

### Puntos Claves de la Aplicación
##### Métodos Synchronized
    Se utilizo metodos de este tipo en las clases entrada y salida, debido a que era necesario indicar al flujo que requiere cada una de las acciones y el momento en que entran en espera al no encontrar paquetes o al encontrar los estantes llenos. 
    
    Aca se muestrá la clase synchronized para meter cajas:
    
```java
public synchronized void meterCaja() throws InterruptedException {
        this.estante.espera_entregar++;
        while (this.estante.casillas == 0) {
        }
        this.estante.espera_entregar--;
        this.estante.casillas--;
    }
}
```

##### Ejecución del Hilo de Cada Objeto
    Cada vez que se escribe una clase hilo se extiende de la clase Thread
    
```java
public class NombreClase extends Thread {
   
}
```
    Adicional que cada clase debe contener el siguiente metodo el cual tiene que crear por su extensión de la clase Thread
    
```java
@Override
    public void run() {
        // código que se desea ejecutar en el hilo
    }
``` 
