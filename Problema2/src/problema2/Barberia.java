package problema2;

import java.awt.Color;

public class Barberia extends Thread {

    final private int sillasDisponibles = 20;
    final private int barberosDisponibles = 2;
    private Cliente[] clientes = new Cliente[sillasDisponibles];
    private Barbero[] barberos = new Barbero[barberosDisponibles];
    public BarberiaFrame frame;

    public Barberia(BarberiaFrame f) {
        this.frame = f;
        //inicializamos el número de barberos
        for (int i = 0; i < this.barberosDisponibles; i++) {
            //this.barberos[i] = new Barbero("Barbero" + Integer.toString(i), 60, this);
            this.barberos[i] = new Barbero("Barbero" + Integer.toString(i), ((int) (Math.random() * (21 - 10)) + 10), this);
        }
        //inicializamos las silla en NULL para que esten disponibles
        for (int i = 0; i < this.sillasDisponibles; i++) {
            this.clientes[i] = null;
        }
    }

    public Barbero[] getBarberos() {
        return this.barberos;
    }

    @Override
    public void run() {
        for (Barbero barbero : this.barberos) {
            //se inicia el hilo de cada barbero
            barbero.start();
        }
        while (true);
    }
    
    public Cliente getPrimero(){
        return clientes[0];
    }

    public synchronized void iniciarPintado() {
        frame.peluquero1.setBackground(Color.red);
        frame.peluquero2.setBackground(Color.red);
        frame.cliente1.setText("");
        frame.cliente2.setText("");
    }

    public synchronized Cliente getSiguienteCliente() {
        for (int i = 0; i < barberos.length; i++) {
            Barbero barbero = barberos[i];
            if (barbero.duerme()) {
                Cliente c = clientes[0];
                if(c==null) 
                    return null;
                sacarCliente();
                System.out.println("Sale de la cola el cliente " + c.getName());
                return c;
            }
        }
        return null;
    }

    public synchronized void sacarCliente() {
        for (int i = 1; i < sillasDisponibles; i++) {
            clientes[i - 1] = clientes[i];
        }
        clientes[sillasDisponibles-1] = null;
        pintarSillas();
    }
    
    public synchronized void pintarSillas(){
        for (int i = 0; i < sillasDisponibles; i++) {
            String nombre = "";
            if (clientes[i] != null) {
                nombre = clientes[i].obtenerNombre();
            }

            switch (i) {
                case 0:
                    frame.silla0.setText(nombre);
                    break;
                case 1:
                    frame.silla1.setText(nombre);
                    break;
                case 2:
                    frame.silla2.setText(nombre);
                    break;
                case 3:
                    frame.silla3.setText(nombre);
                    break;
                case 4:
                    frame.silla4.setText(nombre);
                    break;
                case 5:
                    frame.silla5.setText(nombre);
                    break;
                case 6:
                    frame.silla6.setText(nombre);
                    break;
                case 7:
                    frame.silla7.setText(nombre);
                    break;
                case 8:
                    frame.silla8.setText(nombre);
                    break;
                case 9:
                    frame.silla9.setText(nombre);
                    break;
                case 10:
                    frame.silla10.setText(nombre);
                    break;
                case 11:
                    frame.silla11.setText(nombre);
                    break;
                case 12:
                    frame.silla12.setText(nombre);
                    break;
                case 13:
                    frame.silla13.setText(nombre);
                    break;
                case 14:
                    frame.silla14.setText(nombre);
                    break;
                case 15:
                    frame.silla15.setText(nombre);
                    break;
                case 16:
                    frame.silla16.setText(nombre);
                    break;
                case 17:
                    frame.silla17.setText(nombre);
                    break;
                case 18:
                    frame.silla18.setText(nombre);
                    break;
                case 19:
                    frame.silla19.setText(nombre);
                    break;
                default:
                    break;
            }

        }
    }
    
    public synchronized void ingresarSalaEspera(Cliente cliente) {
        //se busca espacio en la cola de espera
        int contador = 0;
        boolean ingresaACola = false;
        for (int i = 0; i < sillasDisponibles; i++) {
            Cliente c = this.clientes[i];
            if (c == null) {
                this.clientes[i] = cliente;
                ingresaACola = true;
                break;
            }
            contador++;
        }
        if (ingresaACola) {
            System.out.println("Cliente " + cliente.getName() + ", Ingresa a la sala de espera y se sienta en la silla " + contador);
        } else {
            System.out.println("Cliente " + cliente.getName() + ", sale de la peluqueria ya que no hay salas disponibles");
        }
        pintarSillas();
    }

}
