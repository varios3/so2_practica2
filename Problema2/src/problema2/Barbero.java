package problema2;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Barbero extends Thread {

    private boolean duerme;
    private final Barberia barberia;
    private final int tiempoCorte;

    public Barbero(String nombre, int tiempoCorteSegundos, Barberia barberia) {
        this.duerme = false;
        this.setName(nombre);
        this.barberia = barberia;
        this.tiempoCorte =  tiempoCorteSegundos;
    }

    public boolean duerme() {
        return duerme;
    }

    @Override
    public void run() {
        try {
            dormir();
        } catch (InterruptedException ex) {
            Logger.getLogger(Barbero.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (true) {
            if (!duerme) {
                try {
                    this.dormir();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Barbero.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public synchronized void dormir() throws InterruptedException {
        System.out.println("..Barbero " + getName() + " esta durmiendo!");
        duerme = true;

        if (barberia.getPrimero() == null) {
            if (getName().equals("Barbero0")) {
                barberia.frame.peluquero1.setBackground(Color.red);
            } else {
                barberia.frame.peluquero2.setBackground(Color.red);
            }
        }
        wait();
    }

    public synchronized void despertar() {
        System.out.println("..Barbero " + getName() + " ya no esta durmiendo!");
        duerme = false;
        notify();
    }

    public synchronized void cortarPelo(String name) throws InterruptedException {
        if (getName().equals("Barbero0")) {
            barberia.frame.cliente1.setText(name);
        } else {
            barberia.frame.cliente2.setText(name);
        }
        if (getName().equals("Barbero0") && barberia.frame.peluquero1.getBackground()==Color.red) {
            Thread.sleep(500);
            barberia.frame.peluquero1.setBackground(Color.blue);
        } else {
            if(barberia.frame.peluquero2.getBackground()==Color.red)
            {
                Thread.sleep(500);
                barberia.frame.peluquero2.setBackground(Color.blue);
            }
        }
        System.out.println("....cortando el pelo cliente no." + name);
        Thread.sleep(tiempoCorte * 1000);
        System.out.println("....se termino de cortar el pelo no. " + name);
        if (getName().equals("Barbero0")) {
            barberia.frame.cliente1.setText("");
        } else {
            barberia.frame.cliente2.setText("");
        }
    }

}
