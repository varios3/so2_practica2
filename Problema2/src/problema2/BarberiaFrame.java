package problema2;

public class BarberiaFrame extends javax.swing.JFrame {
    
    public BarberiaFrame() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        sala = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        silla0 = new javax.swing.JLabel();
        silla1 = new javax.swing.JLabel();
        silla3 = new javax.swing.JLabel();
        silla2 = new javax.swing.JLabel();
        silla4 = new javax.swing.JLabel();
        silla5 = new javax.swing.JLabel();
        silla6 = new javax.swing.JLabel();
        silla7 = new javax.swing.JLabel();
        silla8 = new javax.swing.JLabel();
        silla9 = new javax.swing.JLabel();
        silla10 = new javax.swing.JLabel();
        silla11 = new javax.swing.JLabel();
        silla12 = new javax.swing.JLabel();
        silla13 = new javax.swing.JLabel();
        silla14 = new javax.swing.JLabel();
        silla15 = new javax.swing.JLabel();
        silla16 = new javax.swing.JLabel();
        silla17 = new javax.swing.JLabel();
        silla18 = new javax.swing.JLabel();
        silla19 = new javax.swing.JLabel();
        peluqueros = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        peluquero1 = new javax.swing.JLabel();
        peluquero2 = new javax.swing.JLabel();
        cliente2 = new javax.swing.JLabel();
        cliente1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Barberia Grupo 8");
        setBackground(new java.awt.Color(0, 0, 0));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 102));
        jLabel1.setText("Barberia");

        jPanel2.setBackground(new java.awt.Color(0, 0, 153));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 442, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 21, Short.MAX_VALUE)
        );

        jPanel3.setBackground(java.awt.Color.red);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 454, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        sala.setBackground(java.awt.Color.lightGray);
        sala.setForeground(new java.awt.Color(204, 204, 204));

        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 102));
        jLabel4.setText("Sala de Espera");

        silla0.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla0.setForeground(new java.awt.Color(0, 0, 102));
        silla0.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla0.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla0.setName(""); // NOI18N

        silla1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla1.setForeground(new java.awt.Color(0, 0, 102));
        silla1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla1.setName(""); // NOI18N

        silla3.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla3.setForeground(new java.awt.Color(0, 0, 102));
        silla3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla3.setName(""); // NOI18N

        silla2.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla2.setForeground(new java.awt.Color(0, 0, 102));
        silla2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla2.setName(""); // NOI18N

        silla4.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla4.setForeground(new java.awt.Color(0, 0, 102));
        silla4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla4.setName(""); // NOI18N

        silla5.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla5.setForeground(new java.awt.Color(0, 0, 102));
        silla5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla5.setName(""); // NOI18N

        silla6.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla6.setForeground(new java.awt.Color(0, 0, 102));
        silla6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla6.setName(""); // NOI18N

        silla7.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla7.setForeground(new java.awt.Color(0, 0, 102));
        silla7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla7.setName(""); // NOI18N

        silla8.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla8.setForeground(new java.awt.Color(0, 0, 102));
        silla8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla8.setName(""); // NOI18N

        silla9.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla9.setForeground(new java.awt.Color(0, 0, 102));
        silla9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla9.setName(""); // NOI18N

        silla10.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla10.setForeground(new java.awt.Color(0, 0, 102));
        silla10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla10.setName(""); // NOI18N

        silla11.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla11.setForeground(new java.awt.Color(0, 0, 102));
        silla11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla11.setName(""); // NOI18N

        silla12.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla12.setForeground(new java.awt.Color(0, 0, 102));
        silla12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla12.setName(""); // NOI18N

        silla13.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla13.setForeground(new java.awt.Color(0, 0, 102));
        silla13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla13.setName(""); // NOI18N

        silla14.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla14.setForeground(new java.awt.Color(0, 0, 102));
        silla14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla14.setName(""); // NOI18N

        silla15.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla15.setForeground(new java.awt.Color(0, 0, 102));
        silla15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla15.setName(""); // NOI18N

        silla16.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla16.setForeground(new java.awt.Color(0, 0, 102));
        silla16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla16.setName(""); // NOI18N

        silla17.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla17.setForeground(new java.awt.Color(0, 0, 102));
        silla17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla17.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla17.setName(""); // NOI18N

        silla18.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla18.setForeground(new java.awt.Color(0, 0, 102));
        silla18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla18.setName(""); // NOI18N

        silla19.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        silla19.setForeground(new java.awt.Color(0, 0, 102));
        silla19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        silla19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        silla19.setName(""); // NOI18N

        javax.swing.GroupLayout salaLayout = new javax.swing.GroupLayout(sala);
        sala.setLayout(salaLayout);
        salaLayout.setHorizontalGroup(
            salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(salaLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, salaLayout.createSequentialGroup()
                        .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(silla4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(silla1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(silla0, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(silla3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(silla2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(silla9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(silla6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(silla5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(silla8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(silla7, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(18, 18, 18)
                        .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(silla14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(silla11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(silla10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(silla13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(silla12, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(18, 18, 18)
                        .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(silla19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(silla16, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(silla15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(silla18, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(silla17, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(16, 16, 16))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, salaLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(56, 56, 56))))
        );
        salaLayout.setVerticalGroup(
            salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(salaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(salaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(salaLayout.createSequentialGroup()
                        .addComponent(silla0, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla2, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla3, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla4, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(salaLayout.createSequentialGroup()
                        .addComponent(silla5, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla6, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla7, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla8, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla9, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(salaLayout.createSequentialGroup()
                        .addComponent(silla10, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla11, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla12, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla13, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla14, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(salaLayout.createSequentialGroup()
                        .addComponent(silla15, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla16, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla17, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla18, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(silla19, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 102));
        jLabel5.setText("Peluqueros");

        peluquero1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        peluquero1.setForeground(new java.awt.Color(0, 0, 102));
        peluquero1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        peluquero1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        peluquero1.setName(""); // NOI18N
        peluquero1.setOpaque(true);

        peluquero2.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        peluquero2.setForeground(new java.awt.Color(0, 0, 102));
        peluquero2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        peluquero2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        peluquero2.setName(""); // NOI18N
        peluquero2.setOpaque(true);

        cliente2.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        cliente2.setForeground(new java.awt.Color(0, 0, 102));
        cliente2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cliente2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        cliente2.setName(""); // NOI18N

        cliente1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        cliente1.setForeground(new java.awt.Color(0, 0, 102));
        cliente1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cliente1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        cliente1.setName(""); // NOI18N

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel6.setForeground(java.awt.Color.blue);
        jLabel6.setText("Ocupado");

        jLabel7.setBackground(new java.awt.Color(255, 255, 255));
        jLabel7.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel7.setForeground(java.awt.Color.red);
        jLabel7.setText("Durmiendo");

        javax.swing.GroupLayout peluquerosLayout = new javax.swing.GroupLayout(peluqueros);
        peluqueros.setLayout(peluquerosLayout);
        peluquerosLayout.setHorizontalGroup(
            peluquerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(peluquerosLayout.createSequentialGroup()
                .addGroup(peluquerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(peluquerosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(peluquero2, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cliente2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(peluquerosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(peluquero1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cliente1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(peluquerosLayout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(jLabel5))
                    .addGroup(peluquerosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel6)))
                .addContainerGap(16, Short.MAX_VALUE))
        );
        peluquerosLayout.setVerticalGroup(
            peluquerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(peluquerosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addGroup(peluquerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(peluquerosLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(peluquero1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(peluquerosLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(cliente1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(peluquerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(peluquerosLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(peluquero2, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(peluquerosLayout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addComponent(cliente2, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(peluquerosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(peluqueros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(sala, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(sala, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(peluqueros, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(56, 56, 56)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(282, Short.MAX_VALUE)))
        );

        jLabel1.getAccessibleContext().setAccessibleName("Barberia");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BarberiaFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BarberiaFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BarberiaFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BarberiaFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BarberiaFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel cliente1;
    public javax.swing.JLabel cliente2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    public javax.swing.JLabel peluquero1;
    public javax.swing.JLabel peluquero2;
    public javax.swing.JPanel peluqueros;
    private javax.swing.JPanel sala;
    public javax.swing.JLabel silla0;
    public javax.swing.JLabel silla1;
    public javax.swing.JLabel silla10;
    public javax.swing.JLabel silla11;
    public javax.swing.JLabel silla12;
    public javax.swing.JLabel silla13;
    public javax.swing.JLabel silla14;
    public javax.swing.JLabel silla15;
    public javax.swing.JLabel silla16;
    public javax.swing.JLabel silla17;
    public javax.swing.JLabel silla18;
    public javax.swing.JLabel silla19;
    public javax.swing.JLabel silla2;
    public javax.swing.JLabel silla3;
    public javax.swing.JLabel silla4;
    public javax.swing.JLabel silla5;
    public javax.swing.JLabel silla6;
    public javax.swing.JLabel silla7;
    public javax.swing.JLabel silla8;
    public javax.swing.JLabel silla9;
    // End of variables declaration//GEN-END:variables
}
