package problema2;

public class Cliente extends Thread {

    Barberia barberia;

    public Cliente(Barberia barberia, String name) {
        this.barberia = barberia;
        this.setName(name);
    }
    
    public String obtenerNombre(){
        return this.getName();
    }

    @Override
    public void run() {
        while (true) {
           Barbero[] barberos = barberia.getBarberos();
            for (int i=0; i<barberos.length;i++) {
                Barbero barbero = barberos[i];
                if (barbero.duerme()) {
                    barbero.despertar();
                    try {
                        barbero.cortarPelo(getName());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
             break;
        }
    }
}
