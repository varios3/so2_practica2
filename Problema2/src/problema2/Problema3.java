package problema2;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Problema3 {

    private static int cliente = 0;
    private static final int TIEMPO_LLEGADA = 30;

    public static void main(String[] args) throws InterruptedException {
        BarberiaFrame frame = new BarberiaFrame();
        Barberia barberia = new Barberia(frame);
        frame.show();
        barberia.start();
        barberia.iniciarPintado();
        Runnable llegadaClientes = () -> {
            while (true) {
                try {
                    //esperando que llegue un nuevo cliente
                    //Thread.sleep(TIEMPO_LLEGADA * 1000);
                    Thread.sleep(((int) (Math.random() * (10)) + 1) * 1000);
                    System.out.println("Ingresa el Cliente " + Integer.toString(cliente));
                    Cliente c = new Cliente(barberia, Integer.toString(cliente));
                    barberia.ingresarSalaEspera(c);
                    cliente++;
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Problema3.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        
        Runnable validacionClientes = () -> {
            while (true) {
                try {
                   Thread.sleep(1000);
                    Cliente cliente = barberia.getSiguienteCliente();
                    if (cliente != null) {
                        Thread.sleep(500);
                        cliente.start();
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(Problema3.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        
       
        Thread hiloLlegada = new Thread(llegadaClientes,"LLega de Clientes");
        hiloLlegada.start();
        
        Thread hiloValidacion = new Thread(validacionClientes,"Validar Disponibilidad Peluquero");
        hiloValidacion.start();
        
    }
    
    

}
