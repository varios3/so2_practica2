# Barberos Dormilones
![Barberos Dormilones](https://i.pinimg.com/originals/ff/8f/d2/ff8fd28d69454ac9f71841764578ce23.jpg)

### Diagrama de Clases 📃
![Diagrama de Clases](./Contenido/DiagramaClases.png)

### Descripción de las Clases 📋
##### Barberia
    Clase principal del programa, ya que es aqui donde se lleva la administración de las sillas disponibles y su respectiva lógica, 
    como la administración básica de los barberos que estan atendiendo, se tiene pre configurados parametros 
    para cada uno de ellos, por lo que si se corre unicamente en consolo admitiria mas de 2 barberos, 
    pero el front end unicamente permite 2.

##### Barbero
    Clase en donde se encuentran los procedimientos de cada barbero, en donde dependiendo de un flag "dormido", se sabe si el barbero 
    está ocupado o no y dependiendo de la cola de clientes que haya en la clase Barberia, la cual almacena, se sabe si duerme o no.
    
##### Cliente
    Esta clase es la mas sencilla de todas, ya que unicamente tiene como referencia la clase barberia principal, y dependiendo de la 
    disponibilidad de los barberos, el cliente se corta el pelo o sigue esperando en su fila, cabe recalcar que unicamente existiran 
    22 clientes como total, 20 en cola y 2 que puedan ser atendidos.
    
##### Problema3
    Clase Main del programa, la cual crea 2 hilos para el ingreso de los clientes y el segundo para ejecutar la validación del primer 
    cliente en cola para ver si es atendido o no.
    
##### BarberiaFrame
    Clase que tiene toda la parte visual del programa, la cual dependen de todas las anteriores para su correcto funcionamiento y 
    visualización.
    
### Flujo General de la Aplicación
![Flujo](./Contenido/Flujo.png)

### Puntos Claves de la Aplicación
##### Métodos Synchronized
    Tanto en la clase de Barbero, como la clase barberia se utilizaron metodos de este tipo, debido a que era necesario indicar al flujo
    que era necesario esperar que se ejecutará el método para continuar con la ejecución, esto para no alterar el flujo y el orden que 
    se esperaba en ingreso de los clientes, en su atención y principalmente en los flags que se manejaron para saber la disponiblidad de
    cada barbero.
    
    Aca se muestrá un ejemplo:
    
```java
public synchronized void sacarCliente() {
    for (int i = 1; i < sillasDisponibles; i++) {
        clientes[i - 1] = clientes[i];
    }
    clientes[sillasDisponibles-1] = null;
    pintarSillas();
}
```
    En este ejemplo se muestra que es un tipo de ordenamiento a la lista de sillas, por lo cual es imporante esperar que esto se ejecute
    para seguir con los demas procesos y no afectar el flujo ni la animación.
##### Ejecución del Hilo de Cada Objeto
    Cada clase utilizada de Barbero, Barberia y Cliente extendia de la clase Hilo, por lo cual cada clase tiene está estructura.
    
```java
public class NombreClase extends Thread {
   
}
```
    Adicional que cada clase debe contener el siguiente metodo el cual tiene que crear por su extensión de la clase Thread
    
```java
@Override
    public void run() {
        // código que se desea ejecutar en el hilo
    }
```
    Con el fin de que cada objeto este siempre en constante ejecución, se creo un ciclo while infinito para las 3 clases, donde unicamente
    en la clase Cliente puede salir solo si finaliza su corte, las demas clases nunca se detienen por ser entidades unicas, por lo cual en 
    cada clase se tiene un código principal para su correcta logica, como por ejemplo les dejo el código de las 3 para que se observe la 
    lógica de cada una.
    

```java
//Barbero
    @Override
    public void run() {
        try {
            dormir();
        } catch (InterruptedException ex) {
            Logger.getLogger(Barbero.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (true) {
            if (!duerme) {
                try {
                    this.dormir();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Barbero.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
//Barberia
    @Override
    public void run() {
        for (Barbero barbero : this.barberos) {
            //se inicia el hilo de cada barbero
            barbero.start();
        }
        while (true);
    }
    
//Cliente
@Override
    public void run() {
        while (true) {
           Barbero[] barberos = barberia.getBarberos();
            for (int i=0; i<barberos.length;i++) {
                Barbero barbero = barberos[i];
                if (barbero.duerme()) {
                    barbero.despertar();
                    try {
                        barbero.cortarPelo(getName());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
             break;
        }
    }
```
##### Comunicación Entre Hilos
    La forma en que se llamaba o ejecutaba un hilo desde otro hilo era haciendo uso del comando ** .start() **, este es herado de la 
    clase Thread por lo que irá a ejecutar posteriormente el código que tenga en su ** run() ** cada clase.
    
    Adicional se manejaron banderas  dentro del mismo hilo, por lo cual se utilizaron los mentodos ** notify() ** y **wait()** los cuales
    se usaron para pausar el hilo (wait()) y este se reaperturará cuando ocurriera una llamada desde otro hilo o se notificará que puede
    seguir con el proceso (notify()), esto se uso para el barbero ya que se podia manejar facilmente sus estados de dormir y despertarse
    y con ello estar su hilo renovandose.
   
```java
    public synchronized void dormir() throws InterruptedException {
        duerme = true;
        wait();
    }

    public synchronized void despertar() {
        duerme = false;
        notify();
    }
```
##### Creación de Runnables
    Esto se utilizó para crear metodos alternativos que se definieron dentró del Main principal, dichos metodos se necesitaban correr 
    como hilos, estos son los de ingresarClientes y validación y para los cuales se crearon su respectivo hilo para cada uno, aca el código.
    
```java    
    Runnable llegadaClientes = () -> {
            while (true) {
                try {
                    //esperando que llegue un nuevo cliente
                    //Thread.sleep(TIEMPO_LLEGADA * 1000);
                    Thread.sleep(((int) (Math.random() * (10)) + 1) * 1000);
                    System.out.println("Ingresa el Cliente " + Integer.toString(cliente));
                    Cliente c = new Cliente(barberia, Integer.toString(cliente));
                    barberia.ingresarSalaEspera(c);
                    cliente++;
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Problema3.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        
        Runnable validacionClientes = () -> {
            while (true) {
                try {
                   Thread.sleep(1000);
                    Cliente cliente = barberia.getSiguienteCliente();
                    if (cliente != null) {
                        Thread.sleep(500);
                        cliente.start();
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(Problema3.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        
       
        Thread hiloLlegada = new Thread(llegadaClientes,"LLega de Clientes");
        hiloLlegada.start();
        
        Thread hiloValidacion = new Thread(validacionClientes,"Validar Disponibilidad Peluquero");
        hiloValidacion.start();
```
